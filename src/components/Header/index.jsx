import "./style.scss";
import { useDisclosure } from "@chakra-ui/react";
import { useState } from "react";
import {
  Drawer,
  DrawerBody,
  DrawerHeader,
  DrawerOverlay,
  DrawerContent,
  Button,
} from "@chakra-ui/react";
import { ChakraProvider } from "@chakra-ui/react";
import { Link } from "react-router-dom";
import { HamburgerIcon } from "@chakra-ui/icons";
export function Header() {
  const { isOpen, onOpen, onClose } = useDisclosure();
  const [placement, setPlacement] = useState("right");

  return (
    <div className="w-screen">
      <div className="container">
        <div className="font-bold text-xl ">Libs Charts</div>
        <ChakraProvider>
          <Button   onClick={onOpen}>
            <HamburgerIcon />
          </Button>
          <Drawer 
          
          placement={placement} onClose={onClose} isOpen={isOpen}>
            <DrawerOverlay />
            <DrawerContent>
              <DrawerHeader  borderBottomWidth="1px">
                Bibliotecas Charts
              </DrawerHeader  >
              <DrawerBody >
                <div>
                  <Link to={"/"}>
                    <p className="font-bold text-zinc-600 pb-10 pt-5 hover:text-black">Recharts</p>
                  </Link>
                </div>

                <div>
                  <Link to={"/chart"}>
                    <p className="font-bold text-zinc-600 pb-10 hover:text-black">Chart.js</p>
                  </Link>
                </div>

                <div>
                  <Link to={"/apexcharts"}>
                    <p className="font-bold text-zinc-600 pb-10 hover:text-black">ApexCharts</p>
                  </Link>
                </div>
              </DrawerBody>
            </DrawerContent>
          </Drawer>
        </ChakraProvider>
      </div>
    </div>
  );
}

import React from "react";
import "./style.scss";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Line } from "react-chartjs-2";
ChartJS.register(
  CategoryScale,
  LinearScale,
  PointElement,
  LineElement,
  Title,
  Tooltip,
  Legend
);
export function Chartjs() {
  return (
    <div className="containerChartjs">
      <h1>Chart.js Teste</h1>
      <Line
        className="stylechartjs"
        data={{
          labels: [
            "January",
            "February",
            "March",
            "April",
            "May",
            "June",
            "July",
          ],
          datasets: [
            {
              label: "Dataset 1",
              data: [12, 32, 56, 102, 700, 98, 46],
              borderColor: "rgb(255, 99, 132)",
              backgroundColor: "rgba(255, 99, 132, 0.5)",
            },
            {
              label: "Dataset 2",
              data: [105, 132, 156, 12, 676, 8, 26],
              borderColor: "rgb(35, 41, 46)",
              backgroundColor: "rgba(53, 162, 235, 0.5)",
            },
          ],
        }}
        options={{
          responsive: true,
          plugins: {
            legend: {
              //position: 'top' as const,
            },
            title: {
              display: true,
              text: "Chart.js Line Chart",
            },
          },
        }}
      />
    </div>
  );
}
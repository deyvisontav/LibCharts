import Chart from "react-apexcharts";
import './style.scss'
const data = {
  options: {
    chart: {
      id: "basic-bar",
    },
    xaxis: {
      categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998],
    },
  },
  series: [
    {
      name: "series-1",
      data: [30, 40, 45, 50, 49, 60, 70, 91],
    },
  ],
};
export function ApexCharts() {
  return (
    <div className='containerApex'>
      <h1>
        Apex Chart Teste
      </h1>
      <Chart
       className='stylechart'
        options={data.options}
        series={data.series}
        type="bar"
        width="800"
      />
    </div>
  );
}
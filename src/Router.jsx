import { Route, Routes, BrowserRouter } from "react-router-dom";
import { Recharts } from "./pages/Recharts";
import { Chartjs } from "./pages/Chart.js/index.jsx";
import { ApexCharts } from "./pages/Apex Charts";
import { Header } from "./components/Header";
export function RoutesApp() {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path="/" element={<Recharts />} />
        <Route path="/chart" element={<Chartjs />} />
        <Route path="/apexcharts" element={<ApexCharts />} />
      </Routes>
    </BrowserRouter>
  );
}

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/pages/**/*.{js,ts,jsx,tsx}",
    "./src/components/**/*.jsx",
    "./src/**/*.jsx",
  ],
  theme: {
    extend: {},
  },
  plugins: [],
}
